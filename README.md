#  Mailman

Mailman sends mail. Useful if you want your automation tools to send you emails for alerts and cannot install sendmail or similar tools for some reason.


# Configuration

configuration is stored in a TOML file.  Currently only plain password login is supported

# Usage

	./mailman --help

# Build

    go build mailman.go
