package main

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/akamensky/argparse"
	"log"
	"net/smtp"
	"os"
	"strconv"
)

type Config struct {
	Server   string
	User     string
	Password string
	Port     int
	From     string
}

func main() {
	parser := argparse.NewParser("mailman", "sends email")
	configPath := parser.String("c", "config", &argparse.Options{Required: true, Help: "Path to toml config"})
	subject := parser.String("s", "subject", &argparse.Options{Required: true, Help: "email subject"})
	message := parser.String("m", "message", &argparse.Options{Required: true, Help: "email message"})
	to := parser.String("t", "to", &argparse.Options{Required: true, Help: "recipient"})

	err := parser.Parse(os.Args)
	if err != nil {
		fmt.Print(parser.Usage(err))
	}

	var conf Config
	if _, err := toml.DecodeFile(*configPath, &conf); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%+v\n", conf)

	// Set up authentication information.
	auth := smtp.PlainAuth("", conf.User, conf.Password, conf.Server)

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	msg := []byte("To: " + *to + "\r\n" +
		"Subject: " + *subject + "!\r\n" +
		"\r\n" + *message + "\r\n")
	err = smtp.SendMail(conf.Server+":"+strconv.Itoa(conf.Port), auth, conf.From, []string{*to}, msg)
	if err != nil {
		log.Fatal(err)
	}
}
